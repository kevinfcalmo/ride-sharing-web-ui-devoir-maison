import React from "react"
import Header from "../components/Header/Header"
import Footer from "../components/Footer/Footer"
import Card from '../components/Card/Card'
import CardExtenible from "../components/CardExtensible/CardExtenible"
import classes from '../styles/Home.module.css'
/* Image pouyr la section 1 */
import Image1 from '../public/1.png'
import Image2 from '../public/2.png'
import Image3 from '../public/3.png'
import Image4 from '../public/4.png'
import Iphone from '../public/iphone.png'

/* Image pour la section 2 */
import CardEx1 from '../public/cardEx1.png'
import CardEx2 from '../public/cardEx2.png'
import CardEx3 from '../public/cardEx3.png'

import Image from "next/image"
import Head from "next/head"

export default function Home() {

  const HOW_TERE_WORK = [
    {
      image: Image1
      ,
      title: 'REQUEST A RIDE',
      description: 'Have to reach office or going for shopping ? Just put your current location and destination and search a ride that suits you'
    },
    {
      image: Image2,
      title: 'POST A RIDE',
      description: 'Going somewhere but hate to travel alone - just post your ride details and publish it'
    },
    {
      image: Image3,
      title: 'INSTANT NOTIFICATIONS',
      description: 'Get instant notifications for your rides and be in contact with fellow riders all the time'
    },
    {
      image: Image4,
      title: 'CASHLESS PAYMENT',
      description: 'Payment made easy by using your own Wallet - no more cash to carry'
    },
  ];

  const TERE_BENEFITS = [
    {
      picture: CardEx1,
      title: 'Flexible working hours',
      description: 'You can decide when, and how much time you want to drive.'
    },
    {
      picture: CardEx2,
      title: 'Earnings',
      description: 'By driving with tere you can earn more.'
    },
    {
      picture: CardEx3,
      title: 'Customer support 24/7',
      description: 'Tere is a local service provider and we are proud to support you in your local language. We are proud to be at your service 24/7!'
    },
  ];
  return (
    <>
      <Head>
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Questrial" />
      </Head>
      <Header />
      <main>
        {/* CALL to action */}
        <section className={classes.cta}>
          <section className={classes.cta_content}>
            <p className={classes.cta_text}>
              Let’s go. Get a link to download the app.
            </p>
            <input className={classes.cta_input} type="tel" name="" id="" placeholder="Enter mobile phone number" />
            <button className={classes.cta_button}>apply to drive</button>
          </section>
        </section>
        {/* première section de contenu */}
        <section className={classes.section}>
          <section className={classes.section_content}>
            <section className={classes.section_head}>
              <h2 className={classes.section_title}>HOW <span className={classes.title_span}>TERE</span>WORKS</h2>
              <p className={classes.section_description}>Download and install the tere app. Enter your phone number and make your user account. when approved you may start driving.</p>
            </section>
            <section className="section_cards_works">
              {/* première image */}
              <section className={classes.caroussel}>
                <Image className={classes.caroussel_image} src={Iphone} />
                <section className={classes.caroussel_circle}>
                  <div className={classes.caroussel_circle}></div>
                  <div className={classes.caroussel_circle}></div>
                  <div className={classes.caroussel_circle}></div>
                  <div className={classes.caroussel_circle}></div>
                </section>
              </section>
              {/*  */}
              {HOW_TERE_WORK.map((item, index) => (
                <Card
                  key={index}
                  index={index}
                  picture={item.image}
                  title={item.title}
                  description={item.description}
                />
              ))}
            </section>
          </section>
        </section>
        {/* Seconde section de contenu */}
        <section className={classes.section}>
          <section className={classes.section_content}>
            <section className={classes.section_head}>
              <h2 className={classes.section_title}><span className={classes.title_span}>TERE</span> BENEFITS</h2>
            </section>
            <section className={classes.cardExtensibles}>
              {TERE_BENEFITS.map((item, index) => (
                <CardExtenible
                  key={index}
                  index={index}
                  title={item.title}
                  description={item.description}
                  picture={item.picture}
                />
              ))}
            </section>
          </section>
        </section>
      </main>
      <Footer />
    </>
  )
}
