import Image from 'next/image';
import React from 'react'
import classes from './Button.module.css'

const Button = (props) => {
    const picture = props.picture;
    const content = props.content;
    const className = props.className;
    const type = props.type;
    return (
        <>
            {type === 'header' &&
                <button className={className === 'herobanner1' ? classes.herobanner1 : classes.herobanner2}>
                    {picture && 
                    <Image className={classes.picture_button} 
                    src={picture} />}
                    {content}
                </button>}
            {type === 'footer' && <button className={className === 'footer1' ? classes.footer1 : classes.footer2}>
                {picture && <Image className={classes.picture_button} src={picture} />}
                {content}
            </button>}
        </>
    )
}

export default Button