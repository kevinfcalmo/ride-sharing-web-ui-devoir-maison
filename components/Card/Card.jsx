import Image from 'next/image';
import React from 'react'
import classes from './Card.module.css'

const Card = (props) => {
    const index = props.index;
    const title = props.title;
    const description = props.description
    const picture = props.picture
    return (
        <section className={classes.card}>
            <Image className={classes.card_back} src={picture} />
            <h3 className={classes.card_title}>{title}</h3>
            <p className={classes.card_description}>{description}</p>
        </section>
    )
}

export default Card