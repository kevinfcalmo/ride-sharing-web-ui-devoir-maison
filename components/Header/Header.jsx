import React from 'react'
import classes from './Header.module.css'
import Navigation from '../Navigation/Navigation'
import Image from 'next/image'
import HerobannerImage from '../../public/herobanner-image.png'
import Content from '../Content/Content'
import AppleStore from '../../public/app-store.png'
import PlayStore from '../../public/play-store.png'
import Button from '../Button/Button'

const Header = () => {

    const title = 'DOWNLOAD APP,SAVE MONEY, MAKEFRIENDS!';
    const description = 'Its simple and its free. Play your part in reducing Carbon Footprint and help Mother Nature to sustain its beauty. So what are you waiting for ?  Lets ride together';
    const buttons = [
        {
            picture: PlayStore,
            content: 'DOWNLOAD',
            className: 'herobanner1',
            type: 'header'
        },
        {
            picture: AppleStore,
            content: 'DOWNLOAD',
            className: 'herobanner2',
            type: 'header'
        },
    ]
    return (
        <>
            <Navigation />
            <section className={classes.herobanner}>
                <section className={classes.herobanner_picture}>
                    <Image className={classes.herobanner_picture_img} src={HerobannerImage} />
                </section>

                <Content
                    title={title}
                    description={description}
                    isHerobanner={true}
                />
            </section>
            <section className={classes.herobanner_buttons}>
                {buttons.map((button, index) => (
                    <Button
                        key={index}
                        picture={button.picture}
                        content={button.content}
                        className={button.className}
                        type={button.type}
                    />
                ))}
            </section>
        </>
    )
}

export default Header