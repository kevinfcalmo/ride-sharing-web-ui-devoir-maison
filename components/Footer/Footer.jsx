import Image from 'next/image'
import React from 'react'
import classes from './Footer.module.css'
import footerLogo from '../../public/logo_footer.png'
import AppleStore from '../../public/app-store.png'
import PlayStore from '../../public/play-store.png'
import Button from '../Button/Button'

const Footer = () => {

    const buttons = [
        {
            picture: PlayStore,
            content: 'DOWNLOAD',
            className: 'footer1',
            type: 'footer'
        },
        {
            picture: AppleStore,
            content: 'DOWNLOAD',
            className: 'footer2',
            type: 'footer'
        },
    ]
    return (
        <footer className={classes.footer}>
            <section className={classes.footer_content}>
                <Image src={footerLogo} className={classes.footer_logo} />
                <section className={classes.footer_buttons}>
                    {buttons.map((button, index) => (
                        <Button
                            key={index}
                            picture={button.picture}
                            content={button.content}
                            className={button.className}
                            type={button.type}
                        />
                    ))}
                </section>
                <section className={classes.footer_section}>
                    <h4 className={classes.footer_title}>
                        Be Our Friend
                    </h4>

                    <p className={classes.footer_description}>
                        3, Season Park, Jakarta
                    </p>

                    <p className={classes.footer_description}>
                        support@foodyar.co,id
                    </p>

                    <p className={classes.footer_description}>
                        021 - 1111 - 2222
                    </p>
                </section>
                <section className={classes.footer_section}>
                    <p className={classes.footer_copyrigth}>
                        All Rights Reserved tere by Codematics 2022
                    </p>
                </section>
            </section>


        </footer>
    )
}

export default Footer