import Image from 'next/image';
import React from 'react'
import classes from './CardExtensible.module.css'

const CardExtenible = (props) => {
    const title = props.title;
    const description = props.description;
    const index = props.index;
    const picture = props.picture;
    return (
        <section className={classes.cardEx}>
            <section className={classes.cardEx_head}>
                <h3 className={classes.cardEx_title}><span className={classes.cardEx_title_span}>0{index + 1}.</span> {title}</h3>
            </section>
            <Image className={classes.cardEx_picture} src={picture} />
            <p className={classes.cardEx_description}>{description}</p>
        </section>
    )
}

export default CardExtenible