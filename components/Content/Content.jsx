import React from 'react'
import classes from './Content.module.css'

const Content = (props) => {
    const title = props.title;
    const description = props.description;
    const isHerobanner = props.isHerobanner;
    return (
        <section className={classes.content_section}>
            {isHerobanner ?
                <div className={classes.content}><h1  className={classes.content_herobanner}>{title}</h1></div> :
                <div className={classes.content}><h3 className={classes.content_main}>{title}</h3></div>
            }
            <div className={classes.content}><p  className={classes.content_description}>{description}</p></div>
        </section>
    )
}

export default Content