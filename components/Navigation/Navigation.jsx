import Image from 'next/image'
import React from 'react'
import classes from './Navigation.module.css'
import Logo from '../../public/logo.png'
import HamburgerMenu from '../../public/charm_menu-hamburger.png'

const Navigation = () => {
    const links = ['Home,How tere works,Tere benefits'];
    return (
        <>
            <section className={classes.navigation_desktop}>
                <Image className={classes.logo} src={Logo} />
                <nav className={classes.navigation_links}>
                    {links.map((link, index) => (
                        <li key={index} className={classes.link_desktop}>{link}</li>
                    ))}
                </nav>
                <Image className={classes.mobile_menu} src={HamburgerMenu} />
            </section>
            <section className={classes.navigation_links_mobile}>
            {links.map((link, index) => (
                        <li key={index} className={classes.link_mobile}>{link}</li>
                    ))}
            </section>
        </>

    )
}

export default Navigation